package net.domdiidom.sb.main;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import com.wasteofplastic.askyblock.ASkyBlockAPI;

import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin{
	
	private ConsoleCommandSender console = Bukkit.getConsoleSender();
	private Economy eco;
	
	@Override
	public void onEnable() {
		console.sendMessage("�2[ScoreBoard] Enabled");
		
		if(this.setupEconomy())
			System.out.println("YAY");
		else
			System.out.println("NEY");
		this.onUpdateScoreBoard();
	}
	
	@Override
	public void onDisable() {
		console.sendMessage("[ScoreBoard] Disabled");
	}
	
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        eco = rsp.getProvider();
        return eco != null;
    }
	
	@SuppressWarnings("deprecation")
	private void onUpdateScoreBoard() {

		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				for(Player player : Bukkit.getOnlinePlayers()) {
					ScoreboardManager sbm = Bukkit.getScoreboardManager();
					Scoreboard board = sbm.getNewScoreboard();
					Objective objective = board.registerNewObjective("aaa", "bbb");
					objective.setDisplayName("�b�lSkycraftblock.eu");
					objective.setDisplaySlot(DisplaySlot.SIDEBAR);
					
					Score obj_island_level = objective.getScore("Island Level");
					Score obj_online = objective.getScore("Online");
					Score obj_ping = objective.getScore("Ping");
					Score obj_vote = objective.getScore("Vote");
					Score obj_money = objective.getScore("Money");
					ASkyBlockAPI.getInstance().calculateIslandLevel(player.getUniqueId());
					
					int i_islandLevel = ASkyBlockAPI.getInstance().getIslandLevel(player.getUniqueId());
					int i_online = Bukkit.getOnlinePlayers().size();
					int i_ping = ((CraftPlayer) player).getHandle().playerConnection.player.ping;
					int i_vote = -1;
					int i_money = (int) eco.getBalance(player);
					
					obj_island_level.setScore(i_islandLevel);
					obj_online.setScore(i_online);
					obj_ping.setScore(i_ping);
					obj_vote.setScore(-1);							
					obj_money.setScore((int)eco.getBalance(player));
					player.setScoreboard(board);
					
				}
			}
		}, 0L, 20L);
	}

}
